#!/bin/bash

grep 'error' ./myfile
grep_status=$? # keep exit status of grep command

if [[ $grep_status -eq 0 ]]; then  # 'error' found in file

	# indicate bad commit
	exit 1

elif [[ $grep_status -eq 1 ]]; then  # 'error' NOT found in file

	# indicate good commit
	exit 0

else # error while running grep

	# indicate untestable commit
	# corresponds to 'git bisect skip'
	exit 125

fi
