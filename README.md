# bisect-example-large

A large example to test the Git bisect feature

## Description

If one can write a script to detect if the wanted error is present, the bisecting process can be automated.

In this case we are looking for the commit that introduced the line `error` into `myfile`.

Here is how to do it:

1. Note good and bad commits: `git log --abbrev-commit` (good = e8bf12f ; bad = 054db3c )
2. Write a script that exits with 0 if the current commit is good and exits with 1 if the commit is bad. Don't commit this into the current git repository. Simulate this step by issuing (`cp check_for_error.sh /tmp/check_for_error_untracked.sh`)
3. Start bisecting: `git bisect start 054db3c e8bf12f`
4. Let git detect the first bad commit automatically: `git bisect run /tmp/check_for_error_untracked.sh` This returns a commit hash.
5. Control results by diffing: `git diff $hash^..$hash` (`^` means the previous commit)
6. Stop bisecting: `git bisect reset`
